// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAIy8b-gxIS7YNLvqTJjebAZidPMwEGpkU',
    authDomain: 'controle-ifsp020.firebaseapp.com',
    databaseURL: 'https://controle-ifsp020.firebaseio.com',
    projectId: 'controle-ifsp020',
    storageBucket: 'controle-ifsp020.appspot.com',
    messagingSenderId: '907489943318',
    appId: '1:907489943318:web:2899c3168621657f727a37',
    measurementId: 'G-C46LMV7LFX'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
