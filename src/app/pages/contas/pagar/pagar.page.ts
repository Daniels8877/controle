import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas;

  constructor(
    private service: ContaService
  ) { }

  ngOnInit() {
    this.service.lista('pagar').subscribe(x => this.listaContas = x);
  }

  remove(conta) {
    this.service.remove(conta);
  }
}
